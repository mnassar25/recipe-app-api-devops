#!/bin/sh

set -e 

python manage.py collectstatic --noinput
python manage.py wait_for_db # pause DB 
python manage.py migrate # run db migrations

uwsgi --socket :9000 --workers 4 --master --enable-threads --module app.wsgi # run wsgi at 9000, 4 workers in the container, master flag run this as master service in terminal to gracfully exit if exit is issued, module app uwsgi is going to run 